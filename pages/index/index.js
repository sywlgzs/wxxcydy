//index.js
//获取应用实例
var config = require('../../config.js')
var http = require('../../utils/http.js')

const app = getApp()
var number = 1
var isLoading = false

Page({
  onShareAppMessage(options) {
    console.log(options.webViewUrl)
  }, 

  data: {
    imgUrls: [],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000, 
    hidden:false, 
    mod: [
      'aspectFit',
      'widthFix',
    ],   
    productList: [], // 列表显示的数据源
    windowHeight: "700px",
    loadingHidden: true,
    lodingInfo: "加载更多",
    totalPage: 0,
  },

  imageLoad: function (e) {
    var res = wx.getSystemInfoSync();
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      ratio = imgwidth / imgheight;
    this.setData({
      bannerHeight: res.windowWidth / ratio
    })
  },

  // 页面初始化 options为页面跳转所带来的参数
  onLoad: function (options) {
      number = 1;
      this.getSlide();
      this.getData();
  },

  //页面渲染完成
  onReady: function () { 
     this.setData({ 
        hidden: true
     }) 
  },

  //获取轮播图列表
  getSlide: function () {
    var that = this;
    http.httpGet("Index-getSlide", { appid: config.APPID }, function (res) {     
      if(res.code==1){
        that.setData({
          imgUrls: res.data
        })
      }      
    });
  },

  // 获取数据  pageIndex：页码参数
  getData: function () {
    var self = this;
    http.httpGet("Index-getProduct", {p:number, appid: config.APPID }, function (res) {
      if (res.code == 1) {
        self.setData({
          productList: res.data.list,
          totalPage: res.data.totalPages
        })
      }
    })
  },
  
  //滑到底部监听事件
  lower: function (e) {
    var that = this;
    if (number < this.data.totalPage && !isLoading) {
      isLoading = true;
      that.setData({
        loadingHidden: false
      })
      wx.request({
        method: 'GET',
        url: config.HTTP_BASE_URL + 'Index-getProduct',
        data: {
          appid: config.APPID,
          p: ++number
        },
        success: function (res) {
          setTimeout(function(){
            that.setData({
              productList: that.data.productList.concat(res.data.data.list), 
              lodingInfo: "加载更多...",             
            });
          },1000)         
        },
        fail: function (error) {
          number--;
          console.log(error);
          that.setData({
            lodingInfo: "加载失败",
          })
        },
        complete: function () {
          isLoading = false;
          //that.setData({
            //lodingInfo: "加载更多...",
          //})
        }
      })
    }else{
      that.setData({
        lodingInfo: "加载完成",
      })
    }

  },


  
  
})
